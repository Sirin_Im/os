import os
import random
import subprocess
import sys
import time

try:
    num_children = int(sys.argv[1])
except ValueError:
    print("Invalid number of children")
    sys.exit(1)

def run_child(child_num):
    child_pid = os.fork()
    if child_pid == 0:
        random_sleep = random.randint(5, 10)
        subprocess.call(["python", "child.py", str(random_sleep)])
        sys.exit(0)
    else:
        print(f"Parent[{os.getpid()}]: I ran children process with PID {child_pid}")
        return child_pid

child_pids = []

for i in range(num_children):
    child_pid = run_child(i)
    child_pids.append(child_pid)

for child_pid in child_pids:
    _, status = os.waitpid(child_pid, 0)
    if os.WIFEXITED(status):
        exit_status = os.WEXITSTATUS(status)
        print(f"Parent[{os.getpid()}]: Child with PID {child_pid} terminated. Exit Status {exit_status}")
        if exit_status != 0:
            print(f"Parent[{os.getpid()}]: Restarting child process.")
            child_pid = run_child(0)

print(f"Parent[{os.getpid()}]: All children processes have terminated.")
