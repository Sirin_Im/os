import os
import random
import sys
import time

try:
    sleep_duration = int(sys.argv[1])
except ValueError:
    print("Invalid sleep duration")
    sys.exit(1)

pid = os.getpid()
ppid = os.getppid()
print(f"Child[{pid}]: I am started. My PID {pid}. Parent PID {ppid}")

time.sleep(sleep_duration)

if random.choice([True, False]):
    exit_status = 0
else:
    exit_status = 1

print(f"Child[{pid}]: I am ended. PID {pid}. Parent PID {ppid}")
sys.exit(exit_status)
