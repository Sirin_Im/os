import random
import time

def main():
    operations = ['+', '-', '*' , '/']
    n = random.randint(120, 180)
    
    for i in range(n):
	x = random.randint(1, 9)
	y = random.randint(1, 9)
	o = random.choice(operations)
	
	print(str(x) + ' ' + o + ' ' + str(y), flush=True)

	time.sleep(1)

if __name__ == "__main__":
    main()